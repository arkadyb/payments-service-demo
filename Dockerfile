########################
######## Build #########
########################

FROM golang:1.11 AS build

COPY . /payments-service-demo
WORKDIR /payments-service-demo

RUN make build

########################
####### Publish ########
########################

FROM alpine:3.7

ARG SERVICE

COPY --from=build /payments-service-demo/dist/payments-service-demo app

CMD ["/app"]
