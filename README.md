# payments-service-demo

---

This is demo service built using go-kit framework.

Service has two REST endpoints to add funds on to account and transfer funds between two accounts.

Use docker compose to start the service `docker-compose up`. This demo service uses file system to store users accounts profiles.
Use `store.Store` interface to implement your own data storage.

Brief structure description:

`cmd`: contains main.go.
Service entry point that reads configuration and inits service dependencies.

`internal/server`: contains all the server related functionality, including server definitions, endpoints declarations and application(service) routines.

`internal/store`: declaration of store service and implementation of JSON data store.

`internal/types`: declaration of data types used across the service.

Root folder contains makefile. Use `make help` command while in root folder to list available commands.
