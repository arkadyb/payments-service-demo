package main

import (
	"bitbucket.org/arkadyb/payments-service-demo/internal/server"
	"bitbucket.org/arkadyb/payments-service-demo/internal/store"
	"os"
	"os/signal"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"
)

var (
	version = "0.0.0-dev"
)

func main() {
	var (
		cfg = server.InitConfig()

		err error
	)

	// Setup log format
	if cfg.LogFormat == strings.ToLower("json") {
		log.SetFormatter(&log.JSONFormatter{})
	}

	// Log version details
	log.WithFields(log.Fields{
		"version": version,
	}).Info("build information")

	fileStore, err := store.NewJsonStore(cfg.BaseFilePath)
	if err != nil {
		log.Fatal("failed to create new data store ", err)
	}

	app, err := server.NewApplication(fileStore)
	if err != nil {
		log.Fatal("failed to create application ", err)
	}

	server := server.NewServer(cfg.Port, app)
	server.Start()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	signal := <-c

	server.Stop()
	log.Fatalf("Process killed with signal: %v", signal.String())
}
