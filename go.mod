module bitbucket.org/arkadyb/payments-service-demo

require (
	github.com/go-kit/kit v0.8.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/gorilla/mux v1.7.0
	github.com/namsral/flag v1.7.4-pre
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.3.0
	github.com/stretchr/testify v1.3.0
)
