package types

type Currency int

const (
	USD Currency = iota
	EUR
	SGD
	PHP
)

type Account struct {
	ID     string
	Amount float64
}
