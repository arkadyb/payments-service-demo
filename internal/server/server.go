package server

import (
	"context"
	"encoding/json"
	"fmt"
	gohttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

func NewServer(port string, app *Application) *Server {
	var (
		addr = fmt.Sprintf(":%s", port)
	)

	router := mux.NewRouter()
	operations := router.PathPrefix("/ops").Subrouter()

	operations.Handle("/transfer", gohttp.NewServer(
		makeTransferFundsEndpoint(app),
		decodeTransferFundsRequest,
		encodeResponse,
	)).Methods("POST")

	operations.Handle("/put", gohttp.NewServer(
		makePutFundsEndpoint(app),
		decodePutFundsRequest,
		encodeResponse,
	)).Methods("POST")

	return &Server{
		app: app,
		Server: &http.Server{
			Addr:    addr,
			Handler: router,
		},
	}
}

type Server struct {
	*http.Server

	app *Application
}

func (s *Server) Stop() {
	log.Println("gracefully stopping server...")
	err := s.Shutdown(context.Background())
	if err != nil {
		log.Error(errors.Wrap(err, "failed to gracefully stop server"))
		return
	}
	log.Println("server has stopped.")
}

func (s *Server) Start() {
	log.Printf("Starting server on %s", s.Addr)
	go func() {
		if err := s.ListenAndServe(); err != nil {
			log.Fatal("unable to start server: ", err)
			time.Sleep(time.Second)
		}
	}()
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}
