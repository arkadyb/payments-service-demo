package server

import (
	"github.com/namsral/flag"
)

type Configuration struct {
	Port string

	BaseFilePath string

	LogFormat string
}

// InitConfig loads configuration from env variables
func InitConfig() *Configuration {
	cfg := &Configuration{}

	flag.StringVar(&cfg.Port, "listen_port", "8085", "The port for the server to listen on")
	flag.StringVar(&cfg.BaseFilePath, "base_file_path", "/Users/arkadybalaba/accounts", "Folder used to store user profiles")
	flag.StringVar(&cfg.LogFormat, "log_format", "text", "Logger format: can be 'text' or 'json'")

	flag.Parse()

	return cfg
}
