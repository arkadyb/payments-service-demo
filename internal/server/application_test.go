package server_test

import (
	"bitbucket.org/arkadyb/payments-service-demo/internal/server"
	"reflect"
	"testing"

	"bitbucket.org/arkadyb/payments-service-demo/internal/store"
	"bitbucket.org/arkadyb/payments-service-demo/internal/types"
	"github.com/stretchr/testify/mock"
)

type MockedStore struct {
	mock.Mock
}

func (m *MockedStore) GetAccount(id string) (account *types.Account, err error) {
	args := m.Called(id)

	if args.Get(0) != nil {
		account = args.Get(0).(*types.Account)
	}

	if args.Get(1) != nil {
		err = args.Error(1)
	}

	return
}
func (m *MockedStore) SaveAccounts(acounts ...*types.Account) (res []*types.Account, err error) {
	args := m.Called(acounts)

	if args.Get(0) != nil {
		res = args.Get(0).([]*types.Account)
	}

	if args.Get(1) != nil {
		err = args.Error(1)
	}

	return
}
func (m *MockedStore) GetExchangeRates() map[types.Currency]float64 {
	j, _ := store.NewJsonStore(".")
	return j.GetExchangeRates()
}

func TestApplication_TransferFunds(t *testing.T) {
	type fields struct {
		store func() store.Store
	}
	type args struct {
		fromID   string
		toID     string
		amount   float64
		currency types.Currency
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []*types.Account
		wantErr bool
	}{
		{
			"Test#1",
			fields{
				store: func() store.Store {
					m := &MockedStore{}
					m.On("GetAccount", "1").Return(&types.Account{
						ID:     "1",
						Amount: 1000,
					}, nil)
					m.On("GetAccount", "2").Return(&types.Account{
						ID:     "2",
						Amount: 1000,
					}, nil)
					m.On("SaveAccounts", mock.Anything).Return([]*types.Account{}, nil)
					return m
				},
			},
			args{
				"1",
				"2",
				5,
				types.EUR,
			},
			[]*types.Account{},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			app, _ := server.NewApplication(tt.fields.store())
			got, err := app.TransferFunds(tt.args.fromID, tt.args.toID, tt.args.amount, tt.args.currency)
			if (err != nil) != tt.wantErr {
				t.Errorf("Application.TransferFunds() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Application.TransferFunds() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestApplication_PutMoney(t *testing.T) {
	type fields struct {
		store func() store.Store
	}
	type args struct {
		accountID string
		amount    float64
		currency  types.Currency
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.Account
		wantErr bool
	}{
		{
			"Test#1",
			fields{
				store: func() store.Store {
					m := &MockedStore{}
					m.On("GetAccount", "1").Return(&types.Account{
						ID:     "1",
						Amount: 1000,
					}, nil)
					m.On("SaveAccounts", mock.Anything).Return([]*types.Account{
						{
							"1",
							1100,
						},
					}, nil)
					return m
				},
			},
			args{
				"1",
				100,
				types.EUR,
			},
			&types.Account{
				"1",
				1100,
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a, _ := server.NewApplication(tt.fields.store())
			got, err := a.PutMoney(tt.args.accountID, tt.args.amount, tt.args.currency)
			if (err != nil) != tt.wantErr {
				t.Errorf("Application.PutMoney() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Application.PutMoney() = %v, want %v", got, tt.want)
			}
		})
	}
}
