package server

import (
	"bitbucket.org/arkadyb/payments-service-demo/internal/types"
	"context"
	"encoding/json"
	"github.com/go-kit/kit/endpoint"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type putFundsRequest struct {
	ID       string         `json:"id"`
	Amount   float64        `json:"amount"`
	Currency types.Currency `json:"currency"`
}

type putFundsResponse struct {
	ID     string  `json:"id"`
	Amount float64 `json:"amount"`
}

func decodePutFundsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request putFundsRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func makePutFundsEndpoint(app *Application) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(putFundsRequest)
		account, err := app.PutMoney(req.ID, req.Amount, req.Currency)
		if err != nil {
			log.Error(errors.Wrap(err, "failed to put funds on to account"))
			return nil, errors.New("failed to put fund on to account")
		}

		return putFundsResponse{
			ID:     account.ID,
			Amount: account.Amount,
		}, nil
	}
}
