package server

import (
	"bitbucket.org/arkadyb/payments-service-demo/internal/store"
	"bitbucket.org/arkadyb/payments-service-demo/internal/types"
	"fmt"
	"github.com/pkg/errors"
)

func NewApplication(store store.Store) (*Application, error) {
	if store == nil {
		return nil, errors.New("store is undefined")
	}

	return &Application{
		store,
	}, nil
}

type Application struct {
	store store.Store
}

func (a *Application) TransferFunds(fromID, toID string, amount float64, currency types.Currency) ([]*types.Account, error) {
	if len(fromID) == 0 {
		return nil, errors.New("'fromID' is undefined")
	}

	if len(toID) == 0 {
		return nil, errors.New("'toID' is undefined")
	}

	if amount == 0 {
		return nil, nil
	}

	var (
		transferAmount = amount * a.store.GetExchangeRates()[currency]
	)

	fromAccount, err := a.store.GetAccount(fromID)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to get 'fromID' account %s", fromID)
	}

	toAccount, err := a.store.GetAccount(toID)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to get 'toID' account %s", toID)
	}

	if fromAccount.Amount-transferAmount < 0 {
		return nil, fmt.Errorf("not enough funds on account %s", fromID)
	}

	fromAccount.Amount -= transferAmount
	toAccount.Amount += transferAmount

	accounts, err := a.store.SaveAccounts(fromAccount, toAccount)
	if err != nil {
		return nil, errors.Wrapf(err, "failed save accounts %s and %s", fromID, toID)
	}

	return accounts, nil
}

func (a *Application) PutMoney(accountID string, amount float64, currency types.Currency) (*types.Account, error) {
	if len(accountID) == 0 {
		return nil, errors.New("'accountID' is undefined")
	}

	if amount < 0 {
		return nil, errors.New("amount cant be negative value")
	}

	account, err := a.store.GetAccount(accountID)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to get account %s", accountID)
	}

	var (
		transferAmount = amount * a.store.GetExchangeRates()[currency]
	)
	account.Amount += transferAmount

	accounts, err := a.store.SaveAccounts(account)
	if err != nil {
		return nil, errors.Wrapf(err, "field to save account %s", accountID)
	}

	return accounts[0], nil
}
