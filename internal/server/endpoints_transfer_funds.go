package server

import (
	"bitbucket.org/arkadyb/payments-service-demo/internal/types"
	"context"
	"encoding/json"
	"github.com/go-kit/kit/endpoint"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type transferFundsRequest struct {
	FromID   string         `json:"from_id"`
	ToID     string         `json:"to_id"`
	Amount   float64        `json:"amount"`
	Currency types.Currency `json:"currency"`
}

type accountResponse struct {
	ID     string  `json:"id"`
	Amount float64 `json:"amount"`
}

type transferFundsResponse struct {
	Accounts []accountResponse `json:"accounts"`
}

func decodeTransferFundsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request transferFundsRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func makeTransferFundsEndpoint(app *Application) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(transferFundsRequest)
		accounts, err := app.TransferFunds(req.FromID, req.ToID, req.Amount, req.Currency)
		if err != nil {
			log.Error(errors.Wrap(err, "failed to transfer funds between account"))
			return nil, errors.New("failed to put fund on to account")
		}

		res := []accountResponse{}
		for _, account := range accounts {
			res = append(res, accountResponse{
				ID:     account.ID,
				Amount: account.Amount,
			})
		}

		return transferFundsResponse{res}, nil
	}
}
