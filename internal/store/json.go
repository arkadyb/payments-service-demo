package store

import (
	"bitbucket.org/arkadyb/payments-service-demo/internal/types"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
	"path"
	"sync"
)

func NewJsonStore(path string) (*JSONStore, error) {
	if path == "" {
		return nil, errors.New("file path is undefined")
	}

	if _, err := os.Stat(path); os.IsNotExist(err) {
		return nil, errors.Wrap(err, "path does not exists")
	}

	return &JSONStore{
		path,
		make(map[string]*sync.Mutex),
	}, nil
}

type JSONStore struct {
	path         string
	mutexPerUser map[string]*sync.Mutex
}

func (j *JSONStore) SaveAccounts(accounts ...*types.Account) ([]*types.Account, error) {
	if len(accounts) == 0 {
		return nil, nil
	}

	// make sure all files are free to access
	lock(j.mutexPerUser, accounts)
	defer unlock(j.mutexPerUser, accounts)

	for _, account := range accounts {
		data, err := json.Marshal(account)
		if err != nil {
			return nil, errors.Wrapf(err, "failed to marshal account for user %s", account.ID)
		}
		err = ioutil.WriteFile(path.Join(j.path, fmt.Sprintf("%s.json", account.ID)), data, 0644)
		if err != nil {
			return nil, errors.Wrapf(err, "failed to save file for user %s", account.ID)
		}
	}

	return accounts, nil
}

func lock(mutexPerUser map[string]*sync.Mutex, accounts []*types.Account) {
	for _, account := range accounts {
		mutex, ok := mutexPerUser[account.ID]
		if !ok {
			mutex = &sync.Mutex{}
			mutexPerUser[account.ID] = mutex
		}

		mutex.Lock()
	}
}

func unlock(mutexPerUser map[string]*sync.Mutex, accounts []*types.Account) {
	for _, account := range accounts {
		if mutex, ok := mutexPerUser[account.ID]; ok {
			mutex.Unlock()
		}
	}
}

func (j *JSONStore) GetAccount(id string) (*types.Account, error) {
	var (
		fname   = path.Join(j.path, fmt.Sprintf("%s.json", id))
		account = &types.Account{
			ID: id,
		}
	)

	if _, err := os.Stat(fname); !os.IsNotExist(err) {
		f, err := os.OpenFile(fname, os.O_CREATE, 0644)
		if err != nil {
			return nil, errors.Wrapf(err, "failed to open file for user %s", id)
		}
		defer f.Close()

		b, err := ioutil.ReadAll(f)
		if err != nil {
			return nil, errors.Wrapf(err, "failed to read from file for user %s", id)
		}

		err = json.Unmarshal(b, account)
		if err != nil {
			return nil, errors.Wrapf(err, "failed to unmarshal account for user %s", id)
		}
	}

	return account, nil
}

func (j *JSONStore) GetExchangeRates() map[types.Currency]float64 {
	return map[types.Currency]float64{
		types.EUR: 74.89,
		types.USD: 66.39,
		types.SGD: 48.9,
		types.PHP: 1.27,
	}
}
