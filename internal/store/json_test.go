package store_test

import (
	"bitbucket.org/arkadyb/payments-service-demo/internal/store"
	"github.com/stretchr/testify/assert"
	"os"
	"reflect"
	"testing"

	"bitbucket.org/arkadyb/payments-service-demo/internal/types"
)

func TestJSONStore_SaveAccounts(t *testing.T) {
	if _, err := os.Stat("./test_data"); os.IsNotExist(err) {
		err := os.Mkdir("./test_data", os.ModeDir)
		assert.NoError(t, err)
	}

	type args struct {
		accounts []*types.Account
	}
	tests := []struct {
		name    string
		args    args
		want    []*types.Account
		wantErr bool
	}{
		{
			"Test#1",
			args{
				[]*types.Account{
					{
						"1",
						1000,
					},
					{
						"2",
						1000,
					},
				},
			},
			[]*types.Account{
				{
					"1",
					1000,
				},
				{
					"2",
					1000,
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j, _ := store.NewJsonStore("./test_data")
			got, err := j.SaveAccounts(tt.args.accounts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("JSONStore.SaveAccounts() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("JSONStore.SaveAccounts() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestJSONStore_SaveAccountsMultiple(t *testing.T) {
	if _, err := os.Stat("./test_data"); os.IsNotExist(err) {
		err := os.Mkdir("./test_data", os.ModeDir)
		assert.NoError(t, err)
	}

	var updateSets = [][]*types.Account{
		{
			{
				"1",
				1000,
			},
			{
				"2",
				1000,
			},
		},
		{
			{
				"1",
				1,
			},
			{
				"3",
				1,
			},
		},
	}

	j, _ := store.NewJsonStore("./test_data")
	for _, updateSet := range updateSets {
		got, err := j.SaveAccounts(updateSet...)
		assert.NoError(t, err)
		assert.EqualValues(t, updateSet, got)
	}

	account1, _ := j.GetAccount("1")
	assert.EqualValues(t, &types.Account{
		"1",
		1,
	}, account1)

	account2, _ := j.GetAccount("2")
	assert.EqualValues(t, &types.Account{
		"2",
		1000,
	}, account2)
}
