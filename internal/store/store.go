package store

import "bitbucket.org/arkadyb/payments-service-demo/internal/types"

// Store describes behaviour of data store
type Store interface {
	GetAccount(id string) (*types.Account, error)
	SaveAccounts(accounts ...*types.Account) ([]*types.Account, error)
	GetExchangeRates() map[types.Currency]float64
}
